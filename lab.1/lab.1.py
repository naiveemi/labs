import math

#блок ввода данных
x = float(input("Введите x: "))
a = float(input("Введите a: "))

#блок расчёта G, F, Y
G = 10*(-45*a**2+49*a*x+6*x**2)/(15*a**2+49*a*x+24*x**2)
F = math.tan(5*a**2+34*a*x+45*x**2)
Y =(-1) * math.asin(7*a**2-a*x-8*x**2)

#блок вывода результатов
print("G=",G, "\nF=",F,"\nY=",Y )


