import math
import matplotlib.pyplot as plt

while True:
    x_lst = []
    y_lst = []
    try:
        choice = int(input('Введите номер функции: '))
        a = float(input("Введите a: "))
        x = float(input("Введите минимальное значение x: "))
        x_stop = float(input("Введите максимальное значение x: "))
        x_step = float(input("Введите величину шага: "))

    except ValueError:
        print('Неверный ввод данных')
        exit(1)

    while x < x_stop:

        if choice == 1:

            try:
                g = 10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
                x_lst.append(x)
                y_lst.append(g)
                print(f'x = {x}, G = {g}')

            except ZeroDivisionError:
                print('Ошибка')
                y_lst.append(None)
                exit(1)
            x += x_step

        elif choice == 2:
            try:
                f = math.tan(5 * a ** 2 + 34 * a * x + 45 * x ** 2)
                x_lst.append(x)
                y_lst.append(f)
                print(f'x = {x}, F = {f}')

            except ValueError:
                print('Ошибка')
                y_lst.append(None)
                exit(1)
            x += x_step

        elif choice == 3:
            try:
                y = math.asin(-7 * a ** 2 + a * x + 8 * x ** 2)
                x_lst.append(x)
                y_lst.append(y)
                print(f'x = {x}, Y = {y}')

            except ValueError:
                print('Ошибка')
                y_lst.append(None)
                exit(1)
            x += x_step

    f_max = max(y_lst)
    f_min = min(y_lst)
    print('f(x) min =', f_min, 'f(x) max=', f_max)

    plt.plot(x_lst, y_lst, 'm*-')
    plt.legend('f(x)')
    plt.title('График')
    plt.xlabel('x |--->')
    plt.ylabel('y |--->')
    plt.show()

    wanna_exit = input('Вы хотите выйти из программы? [Да/Нет] ')
    if wanna_exit in ['Да', 'да']:
        break
    else:
        print('Попробуйте ещё раз')