import math

while True:
    x_lst = []
    y_lst = []

    try:
        data = list(map(float, input('Введите через пробел а,x,х максимальное, размер шага ').split()))
        a = int(data[0])
        x = int(data[1])
        x_stop = int(data[2])
        x_step = int(data[3])
        choice = int(input("Введите номер команды "))
        template = float(input("Введите шаблон: "))

    except ValueError:
        print("Неверный ввод данных")

    while x < x_stop:

        if choice == 1:

            try:
                g = 10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
                x_lst.append(x)
                y_lst.append(g)
                print(f'x = {x}, G = {g}')

            except ZeroDivisionError:
                print('Ошибка')
                x_lst.append(x)
                y_lst.append(None)
                exit(1)
            x += x_step

        elif choice == 2:
            try:
                f = math.tan(5 * a ** 2 + 34 * a * x + 45 * x ** 2)
                x_lst.append(x)
                y_lst.append(f)
                print(f'x = {x}, F = {f}')

            except ValueError:
                print('Ошибка')
                x_lst.append(x)
                y_lst.append(None)
                exit(1)
            x += x_step

        elif choice == 3:
            try:
                y = math.asin(-7 * a ** 2 + a * x + 8 * x ** 2)
                x_lst.append(x)
                y_lst.append(y)
                print(f'x = {x}, Y = {y}')

            except ValueError:
                print('Ошибка')
                x_lst.append(x)
                y_lst.append(None)
                exit(1)
            x += x_step

    output_string = 'f(x) = '

    temp = 0
    for element in y_lst:
        output_string += str(element) + ' '
        if element == template:
            temp += 1

    if len(y_lst) > 0:
        print(output_string)
    print(f'Шаблон встречается = {temp} раз')

    wanna_exit = input('Вы хотите выйти из программы? [Да/Нет] ')
    if wanna_exit in ['Да', 'да']:
        break
    else:
        print('Попробуйте ещё раз')
