import math

fx_lst = [[], [], []]

try:
    data = list(map(float, input('Введите через пробел а,x,х максимальное,размер шага ').split()))
    a = float(data[0])
    x = float(data[1])
    x_stop = float(data[2])
    x_step = float(data[3])

except ValueError:
    print("Ввод некорректен")
    exit(1)

while x < x_stop:

    try:
        g = 10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
        fx_lst[0].append(g)
    except ZeroDivisionError:
        g = None
        fx_lst[0].append(g)

    try:
        f = math.tan(5 * a ** 2 + 34 * a * x + 45 * x ** 2)
        fx_lst[1].append(f)
    except ValueError:
        f = None
        fx_lst[1].append(f)

    try:
        y = math.asin(-7 * a ** 2 + a * x + 8 * x ** 2)
        fx_lst[2].append(y)
    except ValueError:
        y = None
        fx_lst[2].append(y)

    x += x_step

    for i in range(len(fx_lst[0])):
        print(f'{fx_lst[0][i]}\n{fx_lst[1][i]}\n{fx_lst[2][i]}')
