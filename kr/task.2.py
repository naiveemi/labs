import sys
import time

try:
    lines = [] #список построковый изначальный из файла каждая строка отдельный чтот списка и в конце перенос
    for line in sys.stdin:  # Ввод файла
        lines.append(line)

    gif = [] #готовые строки покрашенные
    count = 0

    print("\033c", end="")  # Очистка консоли

    try:
        while True:
            if lines[count].count("```") == 1:
                gif.append("".join(["\033[36m" + lines[i] for i in range(count + 1, count + 24)]))  # Кэширование строк, их "окраска" в синий
                count += 25
            elif lines[count].count("```") != 1:
                count += 1
    except IndexError:
        pass

    del lines

    while True:
        for line in gif:
            time.sleep(0.5)
            print("\033c", end="")  # Вывод элементов списка gif на экран покадрово
            print(line)
except KeyboardInterrupt:
    print('Программа завершена')

