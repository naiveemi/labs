def vectors(vector_x, vector_y, scalar_a):
    return [vector_x_coord * scalar_a + vector_y_coord for vector_x_coord, vector_y_coord in zip(vector_x, vector_y)]


choice = input('Введите int или float: ')

if 'float' in choice:
    vector_x = list(map(float, input('Введите вектор x: ').split()))
    vector_y = list(map(float, input('Введите вектор y: ').split()))
    scalar_a = float(input('Введите a: '))
elif 'int' in choice:
    vector_x = list(map(int, input('Введите вектор x: ').split()))
    vector_y = list(map(int, input('Введите вектор y: ').split()))
    scalar_a = int(input('Введите a: '))

print(f'Результаты: {(vectors(vector_x, vector_y, scalar_a))}')


