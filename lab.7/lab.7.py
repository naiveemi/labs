import math

g_lst = []
f_lst = []
y_lst = []

try:
    data = list(map(float, input('Введите через пробел а,x,х максимальное,размер шага ').split()))
    a = int(data[0])
    x = int(data[1])
    x_stop = int(data[2])
    x_step = int(data[3])

except ValueError:
    print("Ввод некорректен")
    exit(1)

while x < x_stop:
    try:
        g = 10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
        g_lst.append(g)
    except ZeroDivisionError:
        g = None
        g_lst.append(g)

    try:
        f = math.tan(5 * a ** 2 + 34 * a * x + 45 * x ** 2)
        f_lst.append(f)
    except ValueError:
        f = None
        f_lst.append(f)
    try:
        y = math.asin(-7 * a ** 2 + a * x + 8 * x ** 2)
        y_lst.append(y)

    except ValueError:
        y = None
        y_lst.append(y)

    file = open('arrays.txt', 'w')
    file.write(f"{g_lst}\n{y_lst}\n{f_lst}")
    file.close()

    data = []

    file = open('arrays.txt', 'r')
    [data.append(line.split()) for line in file]
    file.close()

    for x in range(len(data[0])):
        print(f"G: f(x) = {data[0][x]}")
    for x in range(len(data[1])):
        print(f"F: f(x) = {data[1][x]}")
    for x in range(len(data[2])):
        print(f"Y: f(x) = {data[2][x]}")

