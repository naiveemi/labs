import time
import matplotlib.pyplot as plt
import random


def area(point, center, radius):
    return (center[0] - point[0]) ** 2 + (center[1] - point[1]) ** 2 <= radius ** 2


def points_counter(points, center, radius):
    output = 0
    for point in points:
        if area(point, center, radius):
            output += 1
    return output


n_min = 10e4
n_max = 10e5
n_step = 10e4

n_data = []
time_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    points = [(random.randint(-50, 50), random.randint(-50, 50)) for i in range(n)]
    center = (random.randint(-50, 50), random.randint(-50, 50))
    radius = n / 4
    for i in range(3):
        start = time.time()
        temp = points_counter(points, center, radius)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_data.append(n)
    time_data.append(average)

plt.plot(n_data, time_data, 'mo-')
plt.legend('...')
plt.title('График')
plt.xlabel('Количество точек')
plt.ylabel('Время выполнения, с.')
plt.show()


