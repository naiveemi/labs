import random


def area(point, center, radius):
    return (center[0] - point[0]) ** 2 + (center[1] - point[1]) ** 2 <= radius ** 2


def points_counter(points, center, radius):
    output = 0
    for point in points:
        if area(point, center, radius):
            output += 1
    return output


num = int(input('Введите количество точек: '))
radius = float(input('Введите радиус: '))

coordinates = []

for i in range(num):
    coordinates.append((random.randint(-50, 50), random.randint(-50, 50)))

center = (random.uniform(0, 50), random.uniform(0, 50))
print(f'Найдено точек: {points_counter(coordinates, center, radius)}')
