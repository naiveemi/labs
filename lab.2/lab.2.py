import math

# блок ввода данных
try:
    c = int(input('Введите номер функции: '))
    x = float(input("Введите x: "))
    a = float(input("Введите a: "))
except ValueError:
    print('Неверный ввод данных')
    exit(1)

# блок расчёта G, F, Y

if c == 1:
    try:
        G = 10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
        print('G=', G)
    except ZeroDivisionError:
        print('Ошибка')
        exit(1)

elif c == 2:
    try:
        F = math.tan(5 * a ** 2 + 34 * a * x + 45 * x ** 2)
        print('F=', F)
    except ValueError:
        print('Ошибка')
    exit(1)

elif c == 3:
    try:
        Y = math.asin(-7 * a ** 2 + a * x + 8 * x ** 2)
        print('Y=', Y)
    except ValueError:
        print('Ошибка')
        exit(1)
